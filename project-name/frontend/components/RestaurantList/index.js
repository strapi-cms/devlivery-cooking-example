import {useEffect, useState} from "react"

import Link from "next/link";

import api from '../../lib/api'

import {
  Button,
  Card,
  CardBody,
  CardColumns,
  CardImg,
  CardSubtitle
} from "reactstrap";
import { CardText, CardTitle, Col, Row } from "reactstrap";

export default ({search}) => {

  const [restaurants, setRestaurants] = useState([])
  const [error, setError] = useState(null)

  useEffect(() => {
    api.get('restaurants')
    .then(resp => setRestaurants(resp.data))
    .catch(err => setError(err))
  }, [])

  if (error) return "Error loading restaurants";

  if (restaurants.length)
  return (
    <div>
      <div className="h-100 d-flex">
      {
        restaurants.map(res => {
          return (
            <Card
              style={{ width: "30%", margin: "0 10px" }}
              key={res.id}
            >
              <CardImg
                top={true}
                style={{ height: 250 }}
                src={`http://localhost:1337${res.image.url}`}
              />
              <CardBody>
                <CardTitle>{res.name}</CardTitle>
                <CardText>{res.description}</CardText>
              </CardBody>
              <div className="card-footer">
                <Link
                  as={`/restaurants/${res.id}`}
                  href={`/restaurants?id=${res.id}`}
                >
                  <a className="btn btn-primary">View</a>
                </Link>
              </div>
            </Card>
          )
        })
      }
      <style jsx global>
        {`
          a {
            color: white;
          }
          a:link {
            text-decoration: none;
            color: white;
          }
          a:hover {
            color: white;
          }
          .card-columns {
            column-count: 3;
          }
        `}
      </style>
      </div>
    </div>
  )

  return <h1>Loading</h1>
};