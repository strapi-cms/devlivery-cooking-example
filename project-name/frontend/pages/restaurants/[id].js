import {useEffect, useState} from 'react'

import {
  Button,
  Card,
  CardBody,
  CardColumns,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle,
  Col,
  Row
} from "reactstrap";
// import Cart from "../components/Cart/Cart";
// import defaultPage from "../hocs/defaultPage";

import api from '../../lib/api'
import { useRouter } from 'next/router'

export default (props) => {

  const router = useRouter()

  const [restaurant, setRestaurant] = useState(null)
  const [error, setError] = useState(null)

  useEffect(() => {
    api.get(`restaurants\\${router.query.id}`)
    .then(resp => setRestaurant(resp.data))
    .catch(err => setError(err))
  }, [])

  const addItem = (item) => {
    // this.props.context.addItem(item);
  }

  if (error) return "Error loading restaurant dishes";

  if (restaurant) {
    return (
      <>
        <h1>{restaurant.name}</h1>
        <Row>
          <Col xs="9" style={{ padding: 0 }}>
            <div style={{ display: "inline-block" }} className="h-100">
              {restaurant.dishes.map(res => (
                <Card
                  style={{ width: "30%", margin: "0 10px" }}
                  key={res.id}
                >
                  <CardImg
                    top={true}
                    style={{ height: 250 }}
                    src={`http://localhost:1337${res.image.url}`}
                  />
                  <CardBody>
                    <CardTitle>{res.name}</CardTitle>
                    <CardText>{res.description}</CardText>
                  </CardBody>
                  <div className="card-footer">
                    <Button
                      onClick={addItem.bind(this, res)}
                      outline
                      color="primary"
                    >
                      + Add To Cart
                    </Button>

                    <style jsx>
                      {`
                        a {
                          color: white;
                        }
                        a:link {
                          text-decoration: none;
                          color: white;
                        }
                        .container-fluid {
                          margin-bottom: 30px;
                        }
                        .btn-outline-primary {
                          color: #007bff !important;
                        }
                        a:hover {
                          color: white !important;
                        }
                      `}
                    </style>
                  </div>
                </Card>
              ))}
            </div>
          </Col>
          {/* <Col xs="3" style={{ padding: 0 }}>
            <div>
              <Cart isAuthenticated={isAuthenticated} />
            </div>
          </Col> */}
        </Row>
      </>
    );
  }

  return <h1>Loading</h1>
}